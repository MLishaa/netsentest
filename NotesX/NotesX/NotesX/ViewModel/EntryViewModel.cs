﻿using NotesX.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace NotesX.ViewModel
{
   public class EntryViewModel : INotifyPropertyChanged
    {

       public Note Note;

      


        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand SaveCommand { get; private set; }
        public ICommand DeleteCommand { get;private  set; }



        public EntryViewModel()
        {
              Note = new Note();
           
            SaveCommand = new Command(SaveNote);
            DeleteCommand = new Command(DeleteNote);
        }

        private async void DeleteNote()
        {
            

            var note =Note;
            await App.Database.DeleteNoteAsync(note);
            await App.Current.MainPage.Navigation.PopAsync();

        }

        private async void SaveNote()
        {
            var note = Note;
            note.Date = DateTime.UtcNow;
            await App.Database.SaveNoteAsync(note);
            await App.Current.MainPage.Navigation.PopAsync();
        }

        public string Text
        {
            get 
            { return Note.Text; }
            set {
               Note.Text = value;
                OnPropertyChanged(nameof(Text));
            }
        }

        
        public void OnPropertyChanged([CallerMemberName] string name=null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

      
    }
}
