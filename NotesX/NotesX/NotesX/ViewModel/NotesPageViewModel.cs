﻿using NotesX.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace NotesX.ViewModel
{
    class NotesPageViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;


        public ICommand NavCommand { get; private set; }

        public NotesPageViewModel()
        {
         
            NavCommand = new Command(Navigate);
          
        }

       
        private async void Navigate()
        {
           
                await App.Current.MainPage.Navigation.PushAsync(new NoteEntryPage
                {
                   
                });
            
        }
     

    }
}
