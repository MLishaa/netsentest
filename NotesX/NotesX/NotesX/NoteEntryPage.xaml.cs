﻿using System;
using System.IO;
using Xamarin.Forms;
using NotesX.Models;

namespace NotesX
{
    public partial class NoteEntryPage : ContentPage
    {
        public NoteEntryPage()
        {
            InitializeComponent();
        }


        async void OnSaveButtonClicked(object sender, EventArgs e)
        {
            //var note = (Note)BindingContext;
            //note.Date = DateTime.UtcNow;
            //await App.Database.SaveNoteAsync(note);
            //await Navigation.PopAsync();
        }

        async void OnDeleteButtonClicked(object sender, EventArgs e)
        {
            Boolean answer = await DisplayAlert("Deletion",
                    "Are you sure ?", "Yes", "No");
            if (answer)
            {
                var note = (Note)BindingContext;
                await App.Database.DeleteNoteAsync(note);
                await Navigation.PopAsync();
            }
            else
            {
                return;
            }
          
        }
    }
}