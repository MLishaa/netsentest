﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFGetStarted
{
   public class Author
    {
        public int AuthorId { get; set; }
        public string Name { get; set; }
        public string  FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        public string  Address { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}
