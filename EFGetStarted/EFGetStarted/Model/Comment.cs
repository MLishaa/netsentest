﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFGetStarted
{
    public class Comment
    {
        public int CommentId { get; set; }
        public string Header { get; set; }
        public string CommentBody { get; set; }

        public int PostId { get; set; }
        public Post  Post { get; set; }


    }
}
