﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFGetStarted
{
    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        // making fully defined one to many relationship between author and posts
        public int AuthorId { get; set; }
        public Author Author { get; set; }

        
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
